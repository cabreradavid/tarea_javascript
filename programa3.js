const readline = require('readline');
const fs = require('fs');

const PATH_ARCHIVO = "archivo_programa2.txt"
const PATH_ARCHIVO_SALIDA = "archivo_programa3.txt"
const archivo_salida = require('fs');

var usuario_cantidad = {};

//para crear un archivo vacío
const fs2 = require('fs')
fs2.closeSync(fs.openSync(PATH_ARCHIVO_SALIDA, 'w'));

var rl = readline.createInterface({
    input: fs.createReadStream(PATH_ARCHIVO)
});

rl.on('line', function(line) {
    texto= String(line).split(';');
    if(!usuario_cantidad[texto[0]]){ //si el usuario aún no se inicializo se agrega sino si ya existe se le suma 1 a la cantidad
        usuario_cantidad[String(texto[0])] = 1;
    }else {
        usuario_cantidad[String(texto[0])] = usuario_cantidad[String(texto[0])]+1;
    }
    
});

rl.on('close', function(line) {
    console.log(usuario_cantidad);
    archivo_salida.appendFile(PATH_ARCHIVO_SALIDA,"Usuario\t\t Cantidad \n--------------------------\n", (err)=>{if(err) console.log(err);});
    for(var usuario in usuario_cantidad){
        archivo_salida.appendFile(PATH_ARCHIVO_SALIDA,usuario+"\t\t"+String(usuario_cantidad[usuario])+"\n", (err)=>{if(err) console.log(err);});
    }
    console.log("Se guardaron correctamente las apariciones de cada usuario en el archivo <<"+PATH_ARCHIVO_SALIDA+">>.");
});
