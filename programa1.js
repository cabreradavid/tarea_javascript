//Comentario agregado para gitlab
var Twit = require('twit'); // importamos el paquete "twit"

var config = require('./config') //importamos los tokens requeridos para acceder a la api de twitter

var T = new Twit(config); //creamos una instancia de Twit para acceder a los métodos 

const archivo_salida = require('fs'); //para guardar en un archivo
const PATH_ARCHIVO = "archivo_programa1.txt"

var moment = require('moment'); //utilizamos la librería moment js para parsear la fecha obtenida de twitter

var usuario;
var fecha;
var texto_tweet;

//para crear un archivo vacío
const fs = require('fs')
fs.closeSync(fs.openSync(PATH_ARCHIVO, 'w'))


var params = {
    q: 'billetaje electrónico',
    count: 50,
    result_type: 'recent',
    tweet_mode: 'extended',
}

T.get('search/tweets', params, buscarDatos);

//La función buscarDatos es una función de devolución de llamada que devuelve los datos cuando hacemos una búsqueda.
function buscarDatos(err, data, response) {
    if (!err){
        console.log(data.statuses.length);
        for(var index = 0; index < data.statuses.length; index++){ //iteramos sobre los datos recibidos de la api de twitter
            //console.log(data.statuses[index]);
            usuario = data.statuses[index].user.screen_name;
            fecha = moment(new Date(data.statuses[index].created_at)).format('h:mm:ss a;YYYY-MM-DD');
            
            if(data.statuses[index].retweeted_status) {
                texto_tweet = "RT @"+data.statuses[index].retweeted_status.user.screen_name+": "+String(data.statuses[index].retweeted_status.full_text).replace(/(\r\n|\n|\r)/gm, "");
                //console.log(texto_tweet+"\n"); 
            }else {
                texto_tweet =  String(data.statuses[index].full_text).replace(/(\r\n|\n|\r)/gm, "");
                //console.log(texto_tweet+"\n");
            }
            archivo_salida.appendFile(PATH_ARCHIVO, usuario+";"+fecha+";"+texto_tweet.replace("\n","")+"\n", (err)=>{if(err) console.log(err);});
        }
        console.log("\nTweets guardados en el archivo <<"+PATH_ARCHIVO+">> correctamente.");
    }else{
        console.log(err);
    }
}