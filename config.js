module.exports = {
    consumer_key: 'YOUR_consumer_key',
    consumer_secret: 'YOUR_consumer_secret',
    access_token: 'YOUR_access_token',
    access_token_secret: 'YOUR_access_token_secret'
  }