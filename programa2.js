const readline = require('readline');
const fs = require('fs');

const PATH_ARCHIVO = "archivo_programa1.txt"
const PATH_ARCHIVO_SALIDA = "archivo_programa2.txt"
const archivo_salida = require('fs');

var usuarios =new Array();
diccionario_tweets = {};

//para crear un archivo vacío
const fs2 = require('fs')
fs2.closeSync(fs.openSync(PATH_ARCHIVO_SALIDA, 'w'))

// create instance of readline
// each instance is associated with single input stream
var rl = readline.createInterface({
    input: fs.createReadStream(PATH_ARCHIVO)
});

// el evento es emitido dps de leer cada linea del archivo
rl.on('line', function(line) {
    texto= String(line).split(';');
    usuarios.push(texto[0]);
    diccionario_tweets[texto[0]]= ";"+texto[1]+";"+texto[2]+";"+texto[3];
});


rl.on('close', function(line) {
    //console.log("desordenado \n",usuarios);
    quicksort(usuarios,0,usuarios.length-1);
    //console.log("ordenado \n",usuarios);
  
    for(var index =0; index < usuarios.length; index++){
        archivo_salida.appendFile(PATH_ARCHIVO_SALIDA,usuarios[index]+diccionario_tweets[usuarios[index]]+"\n", (err)=>{if(err) console.log(err);});
    }
    console.log("Se guardaron correctamente los tweets ordenados en el archivo <<"+PATH_ARCHIVO_SALIDA+">>.");
});

//Basado en el quicksort del libro: Introduction to Algorithms, 3rd Edition.Thomas H. Cormen, Clifford Stein,Ronald L. Rivest
function swap(A, i, j){
    var variable_temp = A[i];
    A[i] = A[j];
    A[j]= variable_temp; 
}

function partition(A, p, r){
    var pivot = A[r];
    var i = p - 1;
    for (var j = p; j < r; j++){
        if ( String(A[j]).toUpperCase() <= String(pivot).toUpperCase() ){  //.slice(0,3)
            i = i+1;
            swap(A,i,j);
        }
    }   
    swap(A,i+1,r);
    return i+1;
}

function quicksort(A, p, r){

    if (p < r){
        q = partition(A, p, r);
        quicksort(A, p, q-1);
        quicksort(A, q+1, r);
    }   
}

